#!/usr/bin/env bash

die() { echo "$*" 1>&2 ; exit 1; }

echo 'Running strikepro-catalog-frontend local development container.'

read -p 'Type host port (default: 4000): ' HOST_PORT
read -p 'Type host project path (default: $(pwd)): ' HOST_PATH

## Checking if $HOST_PATH set
#if [[ -z $HOST_PATH ]]
#then
#    die 'Host project path is required!'
#fi

read -p 'Type docker image tag (default: latest): ' DOCKER_IMAGE_TAG

CURRENT_PROJECT_PATH=$(pwd)
HOST_PORT=${HOST_PORT:-4000}
HOST_PATH=${HOST_PATH:-$PWD}
CONTAINER_PORT=8080
CONTAINER_PATH='/home/node/app'
DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG:-latest}

echo "Run strikepro/catalog-frontend:${DOCKER_IMAGE_TAG}..."
docker run -it -u node -p $HOST_PORT:8080 --name strikepro_catalog_frontend -v "$HOST_PATH":"$CONTAINER_PATH" --rm strikepro/catalog-frontend:$DOCKER_IMAGE_TAG
